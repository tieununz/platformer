#ifndef _MAP_LAYER_H__
#define _MAP_LAYER_H__

#include "PhysicDelegate.h"
#include "../Common/TiledBodyCreator.h"
#include "../B2/Player.h"

class MapLayer : public PhysicDelegate, public TiledBodyCreator
{
public:
	MapLayer();
	virtual ~MapLayer();
	virtual void initMap();
protected:
	Player* _player;
	TMXTiledMap* _map;
public:
	void initTileLayers();
	void initObjectLayers();
private: //tile
	void createFixtures(TMXLayer* layer);
	void createWall(TMXLayer* layer, int x, int y, float width, float height);
	void createGravityScale(TMXLayer* layer, int x, int y, float width, float height);
	void createNormalGravityScale(TMXLayer* layer, int x, int y, float width, float height);
	void createCoin(TMXLayer* layer, int x, int y, float width, float height);
	void createConveryerBelt(TMXLayer* layer, int x, int y, float width, float height);
private: //object
	void addObject(std::string className, ValueMap& properties);
	void addHazards(std::string className, ValueMap& properties);
};

#endif