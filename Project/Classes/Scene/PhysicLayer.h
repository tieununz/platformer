#ifndef _PHYSIC_LAYER_H__
#define _PHYSIC_LAYER_H__

#include "MapLayer.h"

#include "CocosGUI.h"
using namespace ui;
class PhysicLayer : public MapLayer
{
public:
	PhysicLayer();
	virtual ~PhysicLayer();
	virtual bool init();
public:
	virtual void update(float dt) override;
protected:
	void onTouchesBegan(const std::vector<Touch*>& touches, cocos2d::Event  *event);
	void onTouchesMoved(const std::vector<Touch*>& touches, cocos2d::Event  *event);
	void onTouchesEnded(const std::vector<Touch*>& touches, cocos2d::Event  *event);
	void onTouchesCancelled(const std::vector<Touch*>& touches, cocos2d::Event  *event);
	void onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event);
	void onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event);
private:
	bool _keyLeft,_keyRight;
	void selectButton(Ref* pSender, cocos2d::ui::Widget::TouchEventType eEventType);
private:
	float rightMargin, leftMargin, topMargin, bottomMargin;
	bool cameraMoveInProgress;
	void updateCamera(float dt);
private:
	void incSceneZoom();
	void descSceneZoom();
	void resetSceneZoom();
	void setSceneZoom(float newScale);
	Vec2 prevPlayerPos;
	float sceneScale;
	bool _zoomIn,_zoomOut, zoomInProgress;

};

#endif