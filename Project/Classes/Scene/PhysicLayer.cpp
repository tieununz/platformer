#include "PhysicLayer.h"
#include "../B2/b2NodeManager.h"
#include "../Common/GameConfig.h"
#include "../Common/ResourceDefine.h"
#include "../B2/Ball.h"
#define DEFAULT_SCALE 1.0
#define SCREEN_MARGIN_LEFT		0.35f
#define SCREEN_MARGIN_FRONT		0.5f
#define SCREEN_MARGIN_BOTTOM	0.25f
#define SCREEN_MARGIN_TOP		0.35f
#define ZOOM_STEP			0.1f
#define ZOOM_TIME			0.25f

enum kTagButton {
	kTagLeft = 50,
	kTagRight = 51,
	kTagJump = 52,
	kTagLoot = 53,
};

PhysicLayer::PhysicLayer()
{
	_keyLeft = _keyRight = false;

	cameraMoveInProgress = false;
	Size winSize = Director::getInstance()->getWinSizeInPixels();	
	rightMargin = winSize.width - winSize.width * SCREEN_MARGIN_FRONT;
	leftMargin = winSize.width * SCREEN_MARGIN_LEFT;
	topMargin = winSize.height - winSize.height * SCREEN_MARGIN_TOP;
	bottomMargin = winSize.height * SCREEN_MARGIN_BOTTOM;

	_zoomIn = _zoomOut = false;
	zoomInProgress = false;
	sceneScale = DEFAULT_SCALE;
	this->setScale(sceneScale);
}


PhysicLayer::~PhysicLayer()
{
}

bool PhysicLayer::init()
{
	if ( !Layer::init() )
	{
		return false;
	}
	auto touchListener = EventListenerTouchAllAtOnce::create();
	touchListener->onTouchesBegan = CC_CALLBACK_2(PhysicLayer::onTouchesBegan, this);
	touchListener->onTouchesMoved = CC_CALLBACK_2(PhysicLayer::onTouchesMoved, this);
	touchListener->onTouchesEnded = CC_CALLBACK_2(PhysicLayer::onTouchesEnded, this);
	touchListener->onTouchesCancelled = CC_CALLBACK_2(PhysicLayer::onTouchesCancelled, this);

	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener, this);
	auto listener = EventListenerKeyboard::create();
	listener->onKeyPressed = CC_CALLBACK_2(PhysicLayer::onKeyPressed, this);
	listener->onKeyReleased = CC_CALLBACK_2(PhysicLayer::onKeyReleased, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	initWorld();
	initMap();
	initTileLayers();
	initObjectLayers();
	Ball *_ball = Ball::create(Vec2(100,100),_world,this);
	b2NodeManager::getInstance()->addB2Node(_ball);
	//_player = Player::create(VISIBLESIZE/2 + Size(100,100),_world,this);
	//this->runAction(Follow::create(_player, _map->getBoundingBox()));

	//ui::Button *buttonLeft = ui::Button::create(s_pButton_Left,s_pButton_Left,s_pButton_Left,TextureResType::LOCAL);
	//buttonLeft->addTouchEventListener(CC_CALLBACK_2(PhysicLayer::selectButton, this));
	//buttonLeft->setPressedActionEnabled(true);
	//buttonLeft->setTag(kTagLeft);
	//buttonLeft->setPosition(Vec2(150,50));
	//this->addChild(buttonLeft,10);

	return true;
}

void PhysicLayer::update(float dt)
{
	b2NodeManager::getInstance()->update(dt);
	if(_player->getState() == PlayerState::BEFORE_DIE || _player->getState() == PlayerState::DIE)
		return;
	_player->update(dt);
	if(_keyLeft)
	{
		_player->walkLeft(dt);
	}
	if(_keyRight)
	{
		_player->walkRight(dt);
	}
	if(_zoomOut)
	{
		descSceneZoom();
	}
	if(_zoomIn)
	{
		incSceneZoom();
	}
	updateCamera(dt);
}

void PhysicLayer::updateCamera(float dt)
{
	Vec2 realPos = this->convertToWorldSpace(this->_player->getPosition());	
	Vec2 prevPos = this->getPosition();

	float xm = prevPos.x;
	float ym = prevPos.y;

	//	check for X axis
	if (realPos.x >= rightMargin)
		xm -= realPos.x - rightMargin;
	else if (realPos.x <= leftMargin)
		xm += leftMargin - realPos.x;

	//	check for Y axis
	if (realPos.y <= bottomMargin)
		ym += bottomMargin - realPos.y;
	else if (realPos.y >= topMargin)
		ym -= realPos.y - topMargin;

	//	Set X axis independently	
	this->setPosition(xm, ym);
	/*
	//	check for Y axis (only if player is not in air)
	if ( !cameraMoveInProgress )
	{
		float lastYM = ym;
		//	check for Y axis
		if (realPos.y <= bottomMargin)
			ym += bottomMargin - realPos.y;
		else if (realPos.y >= topMargin)
			ym -= realPos.y - topMargin;
		//	If needed ajdust position on Y axis animated 
		if (ym != lastYM)
		{
			this->cameraMoveInProgress = true;
			MoveTo* m1 = MoveTo::create(ZOOM_TIME, ccp(xm, ym));
			CallFunc* toggleCameraProgress  = CallFunc::create([&](){
			cameraMoveInProgress = !cameraMoveInProgress;
			});

			Sequence* seq = Sequence::createWithTwoActions(m1, toggleCameraProgress);
			this->runAction(seq);
		}
	}
	*/

	if(this->getPosition().x >= 0)
	{
		this->setPosition(0,this->getPositionY());
	}
	if(this->getPosition().x < -(_map->getContentSize().width - 960 - HALF_EXTRA_WIDTH * 2))
	{
		this->setPosition(-(_map->getContentSize().width - 960 - HALF_EXTRA_WIDTH * 2),this->getPositionY());
	}
	if(this->getPosition().y >= 0)
	{
		this->setPosition(this->getPositionX(),0);
	}
	if(this->getPosition().y < -(_map->getContentSize().height - 640))
	{
		this->setPosition(this->getPositionX(),-(_map->getContentSize().height - 640));
	}
}

void PhysicLayer::onKeyPressed(EventKeyboard::KeyCode keyCode, Event* event)
{
	switch (keyCode)
	{
	case EventKeyboard::KeyCode::KEY_LEFT_ARROW :
		{
			_keyLeft = true;
		}
		break;
	case EventKeyboard::KeyCode::KEY_RIGHT_ARROW :
		{
			_keyRight = true;
		}
		break;
	case EventKeyboard::KeyCode::KEY_SPACE :
		{
			_player->jump();
		}
		break;
	case EventKeyboard::KeyCode::KEY_X :
		{
			_player->loot();
		}
		break;
	case EventKeyboard::KeyCode::KEY_A :
		{
			_zoomIn = true;
		}
		break;
	case EventKeyboard::KeyCode::KEY_D :
		{
			_zoomOut = true;
		}
		break;
	default:
		break;
	}
}

void PhysicLayer::onKeyReleased(EventKeyboard::KeyCode keyCode, Event* event)
{
	switch (keyCode)
	{
	case EventKeyboard::KeyCode::KEY_LEFT_ARROW :
		{
			_keyLeft = false;
		}
		break;
	case EventKeyboard::KeyCode::KEY_RIGHT_ARROW :
		{
			_keyRight = false;
		}
		break;
	case EventKeyboard::KeyCode::KEY_A :
		{
			_zoomIn = false;
		}
		break;
	case EventKeyboard::KeyCode::KEY_D :
		{
			_zoomOut = false;
		}
		break;
	default:
		break;
	}
}
void PhysicLayer::onTouchesBegan(const std::vector<Touch*>& touches, cocos2d::Event  *event)
{
}
void PhysicLayer::onTouchesMoved(const std::vector<Touch*>& touches, cocos2d::Event  *event)
{
}
void PhysicLayer::onTouchesEnded(const std::vector<Touch*>& touches, Event* event)
{
}
void PhysicLayer::onTouchesCancelled(const std::vector<Touch*>& touches, cocos2d::Event  *event)
{
}

void PhysicLayer::setSceneZoom(float newScale)
{		
	if (zoomInProgress)
		return;

	zoomInProgress = true;

	float currScale = this->getScale();

	ScaleTo* scale = ScaleTo::create(ZOOM_TIME, newScale);

	CallFunc* resetZoomInProgress = CallFunc::create([&](){
		zoomInProgress = false;
	});

	float diffScale = newScale - currScale;

	//	Move to correct position
	Vec2 pos = this->convertToWorldSpace(_player->getPosition());	
	this->setScale(newScale);

	Vec2 posAfterScale = this->convertToWorldSpace(_player->getPosition());	
	this->setScale(currScale);

	Vec2 dif = posAfterScale - pos;

	Vec2 difWorld = this->getPosition() - dif;
	this->runAction(MoveTo::create(ZOOM_TIME, difWorld));

	prevPlayerPos = pos;

	Sequence *s = Sequence::createWithTwoActions(scale, resetZoomInProgress);
	this->runAction(s);

	this->sceneScale = newScale;
}

void PhysicLayer::incSceneZoom()
{
	float newScale = min(2.0f, this->sceneScale + ZOOM_STEP);
	setSceneZoom(newScale);
}

void PhysicLayer::descSceneZoom()
{
	float newScale = max(0.005f, this->sceneScale - ZOOM_STEP);
	setSceneZoom(newScale);
}

void PhysicLayer::resetSceneZoom()
{	
	setSceneZoom(DEFAULT_SCALE);
}

void PhysicLayer::selectButton(Ref* pSender, cocos2d::ui::Widget::TouchEventType eEventType)
{
	Node* node = static_cast<Node*>(pSender);
	int tag = node->getTag();

	switch (eEventType)
	{
	case Widget::TouchEventType::BEGAN:
		{
			switch (tag)
			{
			case kTagLeft:
				{
					_keyLeft = true;
				}
				break;
			case kTagRight:
				{
					_keyRight = true;
				}
				break;
			default:
				break;
			}
		}
		break;
	case Widget::TouchEventType::MOVED:
		break;
	case Widget::TouchEventType::ENDED:
		{
			switch (tag)
			{
			case kTagLeft:
				{
					_keyLeft = false;
				}
				break;
			case kTagRight:
				{
					_keyRight = false;
				}
				break;
			default:
				break;
			}
		}
		break;
	case Widget::TouchEventType::CANCELED:
		break;
	default:
		break;
	}
}
