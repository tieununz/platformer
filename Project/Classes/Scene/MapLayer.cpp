#include "MapLayer.h"
#include "../B2/b2NodeManager.h"
#include "../Common/GameConfig.h"
#include "../B2/Platform/ConveyerBelt.h"
#include "../B2/Platform/CollapsingPlatform.h"
#include "../B2/Platform/RotatingPlatform.h"
#include "../B2/Platform/MovingPlatform.h"
#include "../B2/Hazards/Spiky.h"
#include "../B2/Hazards/RotatingSpiky.h"
#include "../B2/Hazards/MovingSpiky.h"


using namespace std;
#define PTM_RATIO 32.0
MapLayer::MapLayer()
{
}


MapLayer::~MapLayer()
{
}

void MapLayer::initMap()
{
	this->_map = TMXTiledMap::create("Level01.tmx");
	this->addChild(_map,2);
}
//////////////////////////////////////////////////////////////////////////
void MapLayer::initObjectLayers()
{
	// loop over the object groups in this tmx file
	auto objectGroups = _map->getObjectGroups();
	
	b2BodyDef bd;
	for (auto& objectGroup : objectGroups)
	{
		auto objects = objectGroup->getObjects();
		auto name = objectGroup->getGroupName();
		//auto visible = objectGroup->getProperty("Visible").asBool();
		//if(!visible)
		//	break;
		if (strcmp(name.c_str(),"TileBody") == 0)
		{
			auto tileBody = _world->CreateBody(&bd);
			for (auto& object : objects)
			{
				auto valueMap = object.asValueMap();
				{
					auto fixtureShape = createFixture(valueMap);
					if(fixtureShape != NULL) {
						tileBody->CreateFixture(&fixtureShape->fixture);
					}
				}
			}
		}
		if (strcmp(name.c_str(),"Objects") == 0)
		{
			for (auto& object : objects)
			{
				auto properties = object.asValueMap();
				auto type = properties.at("type");
				if (!type.isNull()) {
					if(type.asString() == "Player")
					{
						_player = Player::create(Vec2(properties["x"].asFloat(),properties["y"].asFloat()),_world,this);
					}
					else
					{
						addObject(type.asString(),properties);
					}
				}
			}
		}

		if (strcmp(name.c_str(),"Hazards") == 0)
		{
			for (auto& object : objects)
			{
				auto properties = object.asValueMap();
				auto type = properties.at("type");
				if (!type.isNull()) {
					addHazards(type.asString(),properties);
				}
			}
		}
	}
}

void MapLayer::addObject(std::string type, ValueMap& properties)
{
	if(type == "ConveyerBelt")
	{
		ConveyerBelt::create(properties,_world,this);
	}
	if(type == "CollapsingPlatform")
	{
		CollapsingPlatform::create(properties,_world,this);
	}
	if(type == "RotatingPlatform")
	{
		RotatingPlatform::create(properties,_world,this);
	}
	if(type == "MovingPlatform")
	{
		MovingPlatform::create(properties,_world,this);
	}
}

void MapLayer::addHazards(std::string type, ValueMap& properties)
{
	if(type == "Spiky")
	{
		Spiky::create(properties,_world,this);
	}
	if(type == "RotatingSpiky")
	{
		RotatingSpiky::create(properties,_world,this);
	}
	if(type == "MovingSpiky")
	{
		MovingSpiky::create(properties,_world,this);
	}
	
}
//////////////////////////////////////////////////////////////////////////

void MapLayer::initTileLayers()
{

	for (auto& object : this->_map->getChildren())
	{
		// is this map child a tile layer?
		auto layer = dynamic_cast<TMXLayer*>(object);
		if (layer != nullptr)
			this->createFixtures(layer);
	}
}

void MapLayer::createFixtures(TMXLayer* layer)
{
	// create all the rectangular fixtures for each tile
	Size layerSize = layer->getLayerSize();
	std::string name = layer->getLayerName();

	for (int y = 0; y < layerSize.height; y++)
	{
		for (int x = 0; x < layerSize.width; x++)
		{
			// create a fixture if this tile has a sprite
			auto tileSprite = layer->getTileAt(Point(x, y));
			if (tileSprite && strcmp(name.c_str(),"Wall") == 0)
			{
				this->createWall(layer, x, y, 1.0f, 1.0f);
			}
			if (tileSprite && strcmp(name.c_str(),"ScaleGravity") == 0)
				this->createGravityScale(layer, x, y, 1.0f, 1.0f);
			if (tileSprite && strcmp(name.c_str(),"NormalGravity") == 0)
			{
				layer->setVisible(false);
				this->createNormalGravityScale(layer, x, y, 1.0f, 1.0f);
			}
			if (tileSprite && strcmp(name.c_str(),"TileObject") == 0)
			{
				layer->setVisible(false);
				this->createCoin(layer, x, y, 1.0f, 1.0f);
			}
			if (tileSprite && strcmp(name.c_str(),"ConveryerBelt") == 0)
			{
				//this->createConveryerBelt(layer, x, y, 1.0f, 1.0f);
			}
		}
	}
}

void MapLayer::createWall(TMXLayer* layer, int x, int y, float width, float height)
{
	// get position & size
	auto p = layer->getPositionAt(Point(x,y));
	auto tileSize = this->_map->getTileSize();

	// note: creating the 'world' member variable
	// is discussed in the next chapter

	// create the body
	b2BodyDef bodyDef;
	bodyDef.type = b2_staticBody;
	bodyDef.position.Set(
		(p.x + (tileSize.width / 2.0f)) / PTM_RATIO,
		(p.y + (tileSize.height / 2.0f)) / PTM_RATIO
		);
	b2Body* body = _world->CreateBody(&bodyDef);

	// define the shape
	b2PolygonShape shape;
	shape.SetAsBox(
		(tileSize.width / PTM_RATIO) * 0.5f * width,
		(tileSize.width / PTM_RATIO) * 0.5f * height
		);

	// create the fixture
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits = kFilterCategoryTile;
	fixtureDef.filter.maskBits = 0xffff;
	body->CreateFixture(&fixtureDef);
}

void MapLayer::createGravityScale(TMXLayer* layer, int x, int y, float width, float height)
{
	auto p = layer->getPositionAt(Point(x,y));
	auto tileSize = this->_map->getTileSize();

	// note: creating the 'world' member variable
	// is discussed in the next chapter

	// create the body
	b2BodyDef bodyDef;
	bodyDef.type = b2_staticBody;
	bodyDef.position.Set(
		(p.x + (tileSize.width / 2.0f)) / PTM_RATIO,
		(p.y + (tileSize.height / 2.0f)) / PTM_RATIO
		);
	
	b2Body* body = _world->CreateBody(&bodyDef);

	// define the shape
	b2PolygonShape shape;
	shape.SetAsBox(
		(tileSize.width / PTM_RATIO) * 0.5f * width,
		(tileSize.width / PTM_RATIO) * 0.5f * height
		);

	// create the fixture
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.3f;
	fixtureDef.restitution = 0.0f;
	fixtureDef.filter.categoryBits = kFilterCategoryGravity;
	fixtureDef.filter.maskBits = 0xffff;
	fixtureDef.isSensor = true;
	body->CreateFixture(&fixtureDef);
}

void MapLayer::createNormalGravityScale(TMXLayer* layer, int x, int y, float width, float height)
{
	auto p = layer->getPositionAt(Point(x,y));
	auto tileSize = this->_map->getTileSize();
	// note: creating the 'world' member variable
	// is discussed in the next chapter

	// create the body
	b2BodyDef bodyDef;
	bodyDef.type = b2_staticBody;
	bodyDef.position.Set(
		(p.x + (tileSize.width / 2.0f)) / PTM_RATIO,
		(p.y + (tileSize.height / 2.0f)) / PTM_RATIO
		);

	b2Body* body = _world->CreateBody(&bodyDef);

	// define the shape
	b2PolygonShape shape;
	shape.SetAsBox(
		(tileSize.width / PTM_RATIO) * 0.5f * width,
		(tileSize.width / PTM_RATIO) * 0.5f * height
		);

	// create the fixture
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.3f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits = kFilterCategoryNormalGravity;
	fixtureDef.filter.maskBits = 0xffff;
	fixtureDef.isSensor = true;
	body->CreateFixture(&fixtureDef);
}

void MapLayer::createCoin(TMXLayer* layer, int x, int y, float width, float height)
{

	// get position & size
	auto p = layer->getPositionAt(Point(x,y));
	auto tileSize = this->_map->getTileSize();

	Sprite *_p = Sprite::create("coinGold.png");
	_p->setScale(0.75f);
	_p->setPosition(Vec2(p.x + (tileSize.width / 2.0f),
		p.y + (tileSize.height / 2.0f)));
	this->addChild(_p);

	// note: creating the 'world' member variable
	// is discussed in the next chapter

	// create the body
	b2BodyDef bodyDef;
	bodyDef.type = b2_staticBody;
	bodyDef.position.Set(
		(p.x + (tileSize.width / 2.0f)) / PTM_RATIO,
		(p.y + (tileSize.height / 2.0f)) / PTM_RATIO
		);
	bodyDef.userData = _p;
	b2Body* body = _world->CreateBody(&bodyDef);

	b2CircleShape circle;
	circle.m_radius = tileSize.width / 2 / PTM_RATIO;

	// create the fixture
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circle;
	fixtureDef.filter.categoryBits = kFilterCategoryCoin;
	fixtureDef.filter.maskBits = 0xffff;
	body->CreateFixture(&fixtureDef);
}

void MapLayer::createConveryerBelt(TMXLayer* layer, int x, int y, float width, float height)
{
	auto p = layer->getPositionAt(Point(x,y));
	auto tileSize = this->_map->getTileSize();
	// note: creating the 'world' member variable
	// is discussed in the next chapter

	// create the body
	b2BodyDef bodyDef;
	bodyDef.type = b2_staticBody;
	bodyDef.position.Set(
		(p.x + (tileSize.width / 2.0f)) / PTM_RATIO,
		(p.y + (tileSize.height / 2.0f)) / PTM_RATIO
		);

	b2Body* body = _world->CreateBody(&bodyDef);

	// define the shape
	b2PolygonShape shape;
	shape.SetAsBox(
		(tileSize.width / PTM_RATIO) * 0.5f * width,
		(tileSize.width / PTM_RATIO) * 0.5f * height
		);

	// create the fixture
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &shape;
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.3f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits = kFilterCategoryPlatform;
	fixtureDef.filter.maskBits = 0xffff;
	body->CreateFixture(&fixtureDef);
}
