#include "CollisionListener.h"
#include "cocos2d.h"
#include "../B2/b2NodeManager.h"
#include "../Common/GameConfig.h"
#include "../B2/Player.h"
#include "../B2/Platform/Platform.h"
#include "../B2/Hazards/Hazards.h"

#define PTM_RATIO 32.0
//#define IS_PLAYER(x, y)         (x.type == kGameObjectPlayer || y.type == kGameObjectPlayer)
USING_NS_CC;
CollisionListener::CollisionListener()
{
}


CollisionListener::~CollisionListener()
{
}

void CollisionListener::BeginContact(b2Contact* contact) {

	//b2Body * bodyA = contact->GetFixtureA()->GetBody();
	//b2Body * bodyB = contact->GetFixtureB()->GetBody();
	//uint16 categoryA = contact->GetFixtureA()->GetFilterData().categoryBits;
	//uint16 categoryB = contact->GetFixtureB()->GetFilterData().categoryBits;

	//if(categoryA == kFilterCategoryPlayer && categoryB == kFilterCategoryBall)
	//	contact->SetEnabled(false);
	//if(categoryB == kFilterCategoryPlayer && categoryA == kFilterCategoryBall)
	//	contact->SetEnabled(false);

	b2Body * bodyA = contact->GetFixtureA()->GetBody();
	b2Body * bodyB = contact->GetFixtureB()->GetBody();
	uint16 categoryA = contact->GetFixtureA()->GetFilterData().categoryBits;
	uint16 categoryB = contact->GetFixtureB()->GetFilterData().categoryBits;

	if(categoryA == kFilterCategoryPlayer && categoryB == kFilterCategoryBall)
	{
		//Player * player = (Player *) bodyA->GetUserData();
		//if(!player->_loot)
		contact->SetEnabled(false);

		((b2PhysicObject *) bodyA->GetUserData())->collisionWith(contact,(b2PhysicObject*) bodyB->GetUserData());
	}
	if(categoryB == kFilterCategoryPlayer && categoryA == kFilterCategoryBall)
	{
		//Player * player = (Player *) bodyB->GetUserData();
		//if(!player->_loot)
		contact->SetEnabled(false);

		((b2PhysicObject *) bodyB->GetUserData())->collisionWith(contact,(b2PhysicObject*) bodyA->GetUserData());
	}

	if(categoryA == kFilterCategoryGravity && (categoryB == kFilterCategoryPlayer || categoryB == kFilterCategoryBall))
	{
		bodyB->SetGravityScale(-1);
	}

	if(categoryB == kFilterCategoryGravity && (categoryA == kFilterCategoryPlayer || categoryA == kFilterCategoryBall))
	{
		bodyA->SetGravityScale(-1);
	}

	if(categoryA == kFilterCategoryNormalGravity && (categoryB == kFilterCategoryPlayer || categoryB == kFilterCategoryBall))
	{
		bodyB->SetGravityScale(1);
	}

	if(categoryB == kFilterCategoryNormalGravity && (categoryA == kFilterCategoryPlayer || categoryA == kFilterCategoryBall))
	{
		bodyA->SetGravityScale(1);
	}

	if(categoryA == kFilterCategoryPlayer && categoryB == kFilterCategoryCoin)
	{
		b2NodeManager::getInstance()->addDeleteBody(bodyB);
	}
	if(categoryB == kFilterCategoryPlayer && categoryA == kFilterCategoryCoin)
	{
		b2NodeManager::getInstance()->addDeleteBody(bodyA);
	}

	if(categoryA == kFilterCategoryPlayer && categoryB == kFilterCategoryPlatform)
	{
		Platform * platform = (Platform *) bodyB->GetUserData();
		platform->collisionWith((Player *) bodyA->GetUserData());
	}

	if(categoryB == kFilterCategoryPlayer && categoryA == kFilterCategoryPlatform)
	{
		Platform * platform = (Platform *) bodyA->GetUserData();
		platform->collisionWith((Player *) bodyB->GetUserData());
	}

	if(categoryA == kFilterCategoryPlayer && categoryB == kFilterCategoryHazards)
	{
		Hazards * hazards = (Hazards *) bodyB->GetUserData();
		hazards->collisionWith((Player *) bodyA->GetUserData());
	}

	if(categoryB == kFilterCategoryPlayer && categoryA == kFilterCategoryHazards)
	{
		Hazards * hazards = (Hazards *) bodyA->GetUserData();
		hazards->collisionWith((Player *) bodyB->GetUserData());
	}
}

void CollisionListener::EndContact(b2Contact* contact)
{
	b2Body * bodyA = contact->GetFixtureA()->GetBody();
	b2Body * bodyB = contact->GetFixtureB()->GetBody();
	uint16 categoryA = contact->GetFixtureA()->GetFilterData().categoryBits;
	uint16 categoryB = contact->GetFixtureB()->GetFilterData().categoryBits;

	if(categoryA == kFilterCategoryPlayer && categoryB == kFilterCategoryBall)
	{
		Player * player = (Player *) bodyA->GetUserData();
		player->setBall(NULL);
		contact->SetEnabled(false);
	}
	if(categoryB == kFilterCategoryPlayer && categoryA == kFilterCategoryBall)
	{
		Player * player = (Player *) bodyB->GetUserData();
		player->setBall(NULL);
		contact->SetEnabled(false);
	}

	if(categoryA == kFilterCategoryGravity && (categoryB == kFilterCategoryPlayer || categoryB == kFilterCategoryBall))
	{
		bodyB->SetGravityScale(-1);
	}

	if(categoryB == kFilterCategoryGravity && (categoryA == kFilterCategoryPlayer || categoryA == kFilterCategoryBall))
	{
		bodyA->SetGravityScale(-1);
	}

	if(categoryA == kFilterCategoryNormalGravity && (categoryB == kFilterCategoryPlayer || categoryB == kFilterCategoryBall))
	{
		bodyB->SetGravityScale(1);
	}

	if(categoryB == kFilterCategoryNormalGravity && (categoryA == kFilterCategoryPlayer || categoryA == kFilterCategoryBall))
	{
		bodyA->SetGravityScale(1);
	}
}

void CollisionListener::PreSolve(b2Contact* contact, const b2Manifold* oldManifold)
{
	b2Body * bodyA = contact->GetFixtureA()->GetBody();
	b2Body * bodyB = contact->GetFixtureB()->GetBody();
	uint16 categoryA = contact->GetFixtureA()->GetFilterData().categoryBits;
	uint16 categoryB = contact->GetFixtureB()->GetFilterData().categoryBits;

	if(categoryA == kFilterCategoryPlayer && categoryB == kFilterCategoryBall)
	{
		Player * player = (Player *) bodyA->GetUserData();
		player->setBall((b2PhysicObject *)bodyB->GetUserData());
		contact->SetEnabled(false);
	}
	if(categoryB == kFilterCategoryPlayer && categoryA == kFilterCategoryBall)
	{
		Player * player = (Player *) bodyB->GetUserData();
		player->setBall((b2PhysicObject *)bodyA->GetUserData());
		contact->SetEnabled(false);
	}

	if(categoryA == kFilterCategoryCoin || categoryB == kFilterCategoryCoin)
	{
		contact->SetEnabled(false);
	}

	if(categoryA == kFilterCategoryPlayer && categoryB == kFilterCategoryPlatform)
	{
		Platform * platform = (Platform *) bodyB->GetUserData();
		platform->collisionWith((Player *) bodyA->GetUserData());
	}

	if(categoryB == kFilterCategoryPlayer && categoryA == kFilterCategoryPlatform)
	{
		Platform * platform = (Platform *) bodyA->GetUserData();
		platform->collisionWith((Player *) bodyB->GetUserData());
	}
}

void CollisionListener::PostSolve(b2Contact* contact, const b2ContactImpulse* impulse)
{

}
