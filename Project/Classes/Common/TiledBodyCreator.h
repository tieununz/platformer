#ifndef __TILED_BODY_CREATOR_H__
#define __TILED_BODY_CREATOR_H__
//http://discuss.cocos2d-x.org/t/create-box2d-bodies-with-tmx-objects-layer-cocos2d-x-3-0/13214/5

#include "cocos2d.h"
#include <Box2D/Box2D.h>


class FixtureDef;


class TiledBodyCreator
{
public:
	FixtureDef* createFixture(cocos2d::ValueMap object);
	FixtureDef* createPolygon(cocos2d::ValueMap object);
	FixtureDef* createPolyline(cocos2d::ValueMap object);
	FixtureDef* createCircle(cocos2d::ValueMap object);
	FixtureDef* createRect(cocos2d::ValueMap object);
};

#endif // __TILED_BODY_CREATOR_H__
