#ifndef __GAME_CONFIG_H__
#define __GAME_CONFIG_H__

enum PlayerState
{
	IDLE,
	WALK,
	JUMP,
	//JUMP_WALL,
	SLIDE_WALL,
	BEFORE_DIE,
	DIE
};

enum {
	kBackground = -6,
	kMiddleground = -5,
	kForeground = -4,
	kObject = -3,
	kTut = -2,
	kButton = -1,
};

enum kPhysicObject {
	kPlayer = 100,
	kPlanet = 1,
	kBall = 10,
	kPlatform = 11,
	kHazards = 12,
};

enum
{
	kFilterCategoryPlayer = 0x01,
	kFilterCategoryTile = 0x02,
	kFilterCategorySolidObject = 0x03,
	kFilterCategoryNonSolidObject = 0x04,
	kFilterCategoryGravity = 0x05,
	kFilterCategoryNormalGravity = 0x06,
	kFilterCategoryBall = 0x07,
	kFilterCategoryCoin = 0x08,
	kFilterCategoryPlatform = 0x09,
	kFilterCategoryHazards = 0x10
};

#define VISIBLESIZE Director::getInstance()->getVisibleSize()
#define HALF_EXTRA_WIDTH ((VISIBLESIZE.width - VISIBLESIZE.height * 960 / 640) / 2)

#define max(_x,_y) ((_x)>(_y)?(_x):(_y))
#define min(_x,_y) ((_x)>(_y)?(_y):(_x))

typedef enum {
	POLYGON_FIXTURE,
	POLYLINE_FIXTURE,
	RECT_FIXTURE,
	CIRCLE_FIXTURE,
	UNKNOWN_FIXTURE
} fixtureTypes;

class FixtureDef {
public:
	FixtureDef()
		: next(nullptr) {}

	~FixtureDef() {
		delete next;
		delete fixture.shape;
	}

	FixtureDef *next;
	b2FixtureDef fixture;
	int callbackData;
};

#endif