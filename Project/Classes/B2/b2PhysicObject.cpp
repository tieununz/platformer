#include "b2PhysicObject.h"
#include "../Common/GameConfig.h"
#define PTM_RATIO 32.0
USING_NS_CC;
b2PhysicObject::b2PhysicObject(void)
{
}


b2PhysicObject::~b2PhysicObject(void)
{
}

void b2PhysicObject::update(float dt)
{
	if (_body && isVisible())
	{
		Node::setPosition(Vec2(_body->GetPosition().x * PTM_RATIO,_body->GetPosition().y * PTM_RATIO));
		Node::setRotation(CC_RADIANS_TO_DEGREES(-1 * _body->GetAngle()));
	}
}

void b2PhysicObject::updateContacts()
{
	float heightFactor = this->getContentSize().height / 2;
	heightFactor /= (PTM_RATIO * 1.0f);
	//heightFactor = 0.7f;
	// reset the flags
	_hasContactBelow = _hasContactAbove = _hasContactToTheSide = _hasContactWithObject = _hasContactToTheSide_Left = _hasContactToTheSide_Right = false;

	// iterate over the Box2d body's contacts
	b2ContactEdge* edge = nullptr;
	for (edge = _body->GetContactList(); edge; edge = edge->next)
	{
		if (!edge->contact->IsEnabled() || !edge->contact->IsTouching())
			continue;

		// get vector from the body's position to the contact's
		b2Vec2 v = edge->other->GetPosition() - _body->GetPosition();
		//uint16 cate = edge->other->GetFixtureList()->

		b2Filter filter;
		uint16 cate;
		for ( b2Fixture* f = edge->other->GetFixtureList(); f; f = f->GetNext() ) {
			cate = f->GetFilterData().categoryBits;
		}
		// set flags based on vector
		if (fabsf(v.x) > heightFactor && cate == kFilterCategoryTile)
		{
			_hasContactToTheSide = true;
			if (v.x > heightFactor)
				_hasContactToTheSide_Right = true;

			if (v.x < -heightFactor)
				_hasContactToTheSide_Left = true;
		}
		else if (v.y > heightFactor)
			_hasContactAbove = true;
		else if (v.y < -heightFactor)
			_hasContactBelow = true;

		void* userData = edge->other->GetUserData();
		if (userData)
		{
			auto o = static_cast<b2PhysicObject*>(userData);
			if (o)
				_hasContactWithObject = true;
		}
	}
}