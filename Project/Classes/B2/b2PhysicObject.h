#ifndef _PHYSIC_OBJECT_H__
#define _PHYSIC_OBJECT_H__
#include "b2Node.h"
class b2PhysicObject : public b2Node
{
public:
	b2PhysicObject();
	virtual ~b2PhysicObject();
	virtual void update(float dt) override;
	CC_SYNTHESIZE(int, _type, Type);
	virtual void updateContacts();
	virtual void setProperties(cocos2d::ValueMap& properties) {};
	virtual void collisionWith(b2Contact* contact,b2PhysicObject* object) {};
	bool _hasContactBelow, _hasContactAbove, _hasContactToTheSide, _hasContactToTheSide_Left, _hasContactToTheSide_Right, _hasContactWithObject;
};

#endif