#include "Player.h"
#include "../Common/GameConfig.h"
#include "b2NodeManager.h"
#define PTM_RATIO 32.0
#define MAX_VELOCITY b2Vec2(15.0f,30.0f)
#define IMPULSE_WALK b2Vec2(75.0f,0.0f)
#define IMPULSE_JUMP b2Vec2(20.0f,32.5f)
#define IMPULSE_THROW b2Vec2(20.0f,20.0f)

USING_NS_CC;
Player::Player(void)
{
	_hasLoot = false;
	_ball = NULL;
	_turnLeft = false;
}

Player::~Player(void)
{
	CC_SAFE_RELEASE(walk);
}

Player* Player::create(cocos2d::Vec2 position,b2World* world,Node *node)
{
	Player * sprite = new Player();
	if (sprite) {
		sprite->initPhysic(position,world,node);
		sprite->autorelease();
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}

void Player::initPhysic(cocos2d::Vec2 position,b2World* world,Node *node)
{
	float radius = 0.7f;
	_world = world;
	_type = kBall + 10;
	this->setPosition(position);
	this->setContentSize(Size(radius * 2 * PTM_RATIO,radius * 2 * PTM_RATIO));
	//node->addChild(this,-1,kObject);
	node->addChild(this,_type,kPlayer);

	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(position.x / PTM_RATIO, position.y / PTM_RATIO);
	bodyDef.userData = this;
	bodyDef.fixedRotation = true;

	_body = _world->CreateBody(&bodyDef);

	b2CircleShape circle;
	circle.m_radius = radius ;
	//b2PolygonShape circle;
	//circle.SetAsBox(15 / PTM_RATIO,30 /PTM_RATIO);
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circle;    
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits = kFilterCategoryPlayer;
	fixtureDef.filter.maskBits = 0xffff;
	_body->CreateFixture(&fixtureDef); 
	_body->SetBullet(true);

	_mainSprite = Sprite::create("player/player_idle.png");
	this->addChild(_mainSprite);

	Vector<SpriteFrame*> animFrames(12);
	char str[100] = {0};
	for(int i = 1; i < 12; i++)
	{
		if(i < 10)
			sprintf(str, "player/player_walk%02d.png",i);
		else
			sprintf(str, "player/player_walk%2d.png",i);

		auto frame = SpriteFrame::create(str,Rect(0,0,70,94)); //we assume that the sprites' dimentions are 40*40 rectangles.
		animFrames.pushBack(frame);
	}

	auto animation = Animation::createWithSpriteFrames(animFrames, 0.05f);
	//animation->setRestoreOriginalFrame(true);
	auto animate = Animate::create(animation);
	walk = RepeatForever::create(animate);
	walk->retain();
	//_mainSprite->runAction();
	_state = PlayerState::IDLE;
}

void Player::walkLeft(float dt)
{
	if(	_state == PlayerState::BEFORE_DIE || _state == PlayerState::DIE)
		return;
	b2Vec2 impulse = dt * IMPULSE_WALK;
	impulse.x = -impulse.x;
	this->_body->ApplyLinearImpulse(impulse, _body->GetWorldCenter(), true);

	b2Vec2 currentVelocity = _body->GetLinearVelocity();
	if(currentVelocity.x < -MAX_VELOCITY.x)
	{
		this->_body->SetLinearVelocity(b2Vec2(-MAX_VELOCITY.x,currentVelocity.y));
	}
	_turnLeft = true;
	this->setScaleX(-1);

	if(_hasContactBelow && !_hasContactToTheSide)
	{
		setWalk();
	}
	if(!_hasContactBelow && _hasContactToTheSide && fabsf(currentVelocity.x) < 0.5f )
	{
		setSlideWall();
	}
}


void Player::walkRight(float dt)
{
	if(	_state == PlayerState::BEFORE_DIE || _state == PlayerState::DIE)
		return;
	b2Vec2 impulse = dt * IMPULSE_WALK;
	_body->ApplyLinearImpulse(impulse, _body->GetWorldCenter(), true);

	b2Vec2 currentVelocity = _body->GetLinearVelocity();
	if(currentVelocity.x > MAX_VELOCITY.x)
	{
	_body->SetLinearVelocity(b2Vec2(MAX_VELOCITY.x,currentVelocity.y));
	}
	_turnLeft = false;
	this->setScaleX(1);

	if(_hasContactBelow && !_hasContactToTheSide)
	{
		setWalk();
	}
	if(!_hasContactBelow && _hasContactToTheSide && fabsf(currentVelocity.x) < 0.5f )
	{
		setSlideWall();
	}
}

void Player::jump()
{
	if(	_state == PlayerState::BEFORE_DIE || _state == PlayerState::DIE)
		return;
	updateContacts();
	if(_hasContactBelow)
	{
		countJump = 0;
	}
	if(_hasContactBelow || countJump < 2 || _hasContactToTheSide)
	{
		countJump++;
		b2Vec2 currentVelocity = _body->GetLinearVelocity();
		_body->SetLinearVelocity(b2Vec2(currentVelocity.x,0.0f));
		b2Vec2 impulse(0.0f,0.0f);
		impulse.y = IMPULSE_JUMP.y;
		if(_hasContactToTheSide)
		{
			impulse.x = IMPULSE_JUMP.x;
			if(_hasContactToTheSide_Right)
				impulse.x = -impulse.x;
		}
		_body->ApplyLinearImpulse(impulse, _body->GetWorldCenter(), true);
	}

	if(_state == PlayerState::IDLE || _state == PlayerState::WALK || _state == PlayerState::SLIDE_WALL)
	{
		if(_hasContactBelow && !_hasContactToTheSide)
		{
			setJump();
		}
		if(!_hasContactBelow && _hasContactToTheSide)
		{
			setJump();
		}
		if(_hasContactBelow && _hasContactToTheSide)
		{
			setSlideWall();
		}
	}
}


void Player::setJump()
{
	_state = PlayerState::JUMP;
	_mainSprite->stopAllActions();
	_mainSprite->removeFromParent();
	_mainSprite = Sprite::create("player/player_jump.png");
	this->addChild(_mainSprite);
}
void Player::setWalk()
{
	if(_state == PlayerState::WALK)
		return;
	_state = PlayerState::WALK;
	_mainSprite->stopAllActions();
	_mainSprite->runAction(walk);
}
void Player::setIdle()
{
	_state = PlayerState::IDLE;
	_mainSprite->stopAllActions();
	_mainSprite->removeFromParent();
	_mainSprite = Sprite::create("player/player_idle.png");
	this->addChild(_mainSprite);
}
void Player::setSlideWall()
{
	_state = PlayerState::SLIDE_WALL;
	_mainSprite->stopAllActions();
	_mainSprite->removeFromParent();
	_mainSprite = Sprite::create("player/player_slide_wall.png");
	this->addChild(_mainSprite);
}

void Player::setDie()
{
	if(	_state == PlayerState::BEFORE_DIE || _state == PlayerState::DIE)
		return;
	_state = PlayerState::BEFORE_DIE;
	_mainSprite->stopAllActions();
	_mainSprite->removeFromParent();
	_mainSprite = Sprite::create("player/player_slide_wall.png");
	_mainSprite->setScale(0.75f);
	this->addChild(_mainSprite);
	this->_ignoreBody = true;
	//_body->SetLinearVelocity(b2Vec2_zero);
	//_body->SetAngularVelocity(0);

	DelayTime* delayTime = DelayTime::create(0.25f);
	MoveBy* moveUp = MoveBy::create(0.5f,Vec2(0,100));
	MoveBy* moveDown = MoveBy::create(1.5f,Vec2(0,-960));
	CallFunc* callBack  = CallFunc::create([&](){
		_state = PlayerState::DIE;
	});
	Sequence* seq = Sequence::create(delayTime,moveUp,moveDown, callBack,NULL);
	this->runAction(seq);
}

void Player::loot()
{
	if(	_state == PlayerState::BEFORE_DIE || _state == PlayerState::DIE)
		return;
	if(_ball == NULL)
	{
		return;
	}
	if(_hasLoot)
	{
		updateContacts();
		b2Vec2 impulse = IMPULSE_THROW;
		if(_turnLeft && !_hasContactToTheSide)
			impulse.x = -impulse.x;
		if(_hasContactToTheSide_Right)
			impulse.x = -impulse.x;
		_ball->getBody()->SetLinearVelocity(b2Vec2(0.0f,0.0f));
		_ball->getBody()->ApplyLinearImpulse(impulse,_ball->getBody()->GetWorldCenter(),true);
	}
	_hasLoot = !_hasLoot;
}

void Player::update(float dt)
{
	if(	_state == PlayerState::BEFORE_DIE || _state == PlayerState::DIE)
		return;
	b2PhysicObject::update(dt);
	if(_ball != NULL && _hasLoot)
	{
		_ball->getBody()->SetLinearVelocity(b2Vec2(0.0f,0.0f));
		_ball->setPosition(this->getPosition());
	}

	b2Vec2 currentVelocity = _body->GetLinearVelocity();
	if(currentVelocity.y > MAX_VELOCITY.y)
	{
		_body->SetLinearVelocity(b2Vec2(currentVelocity.x,MAX_VELOCITY.y));
		currentVelocity = _body->GetLinearVelocity();
	}
	if(currentVelocity.y < -MAX_VELOCITY.y)
	{
		_body->SetLinearVelocity(b2Vec2(currentVelocity.x,-MAX_VELOCITY.y));
		currentVelocity = _body->GetLinearVelocity();
	}
	updateContacts();

	b2ContactEdge* edge = nullptr;
	for (edge = _body->GetContactList(); edge; edge = edge->next)
	{
		if (!edge->contact->IsEnabled() || !edge->contact->IsTouching())
			continue;
		void* userData = edge->other->GetUserData();
		if (userData)
		{
			auto o = static_cast<b2PhysicObject*>(userData);
			if (o)
			{
				currentVelocity -= o->getBody()->GetLinearVelocity();
			}
		}
	}

	if(_state == PlayerState::JUMP)
	{
		if(_hasContactBelow && !_hasContactToTheSide && fabsf(currentVelocity.y) < 0.5f )
		{
			setIdle();
		}
	}

	if(_state == PlayerState::JUMP)
	{
		if(!_hasContactBelow && _hasContactToTheSide && fabsf(currentVelocity.x) < 0.5f )
		{
 			setSlideWall();
		}
	}

	if(_state == PlayerState::SLIDE_WALL)
	{
		if(currentVelocity.Length() <= 0.5f)
		{
			setIdle();
		}
	}
	
	if(_state == PlayerState::WALK)
	{
		if(currentVelocity.Length() <= 0.5f)
		{
			setIdle();
		}
	}
}

void Player::collisionWith(b2Contact* contact,b2PhysicObject* object)
{
	if(	_state == PlayerState::BEFORE_DIE || _state == PlayerState::DIE)
		return;
	b2ContactEdge* edge = _body->GetContactList();
	for ( ; edge; edge = edge->next)
	{

		// are we landing on top of this object?
		float heightFactor = this->getContentSize().height / 2;
		heightFactor /= (PTM_RATIO * 1.0f);

		b2Vec2 v = edge->other->GetPosition() - _body->GetPosition();
		float dictance = fabsf(object->getBody()->GetPosition().x - _body->GetPosition().x);

		// if so, attack the object
		if (dictance < (heightFactor * 2 - 2.5f/PTM_RATIO))
		{
			if(v.y < (-heightFactor))
			{
				_body->SetLinearVelocity(b2Vec2(0,0));
				_body->ApplyLinearImpulse(b2Vec2(0,50.0f),_body->GetWorldCenter(),true);

				setJump();
			}
		}
	}
}