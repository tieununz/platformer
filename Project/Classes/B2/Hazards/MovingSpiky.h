#ifndef __MOVING_SPIKY_H__
#define __MOVING_SPIKY_H__
#include "Hazards.h"

class MovingSpiky : public Hazards
{
public:
	MovingSpiky(void);
	virtual ~MovingSpiky(void);
	static MovingSpiky* create(cocos2d::ValueMap& properties,b2World* world,cocos2d::Node *node);
	virtual void update(float dt) override;
	virtual void collisionWith(b2PhysicObject* physicObject);
private:
	void initPhysic(cocos2d::ValueMap& properties,b2World* world,cocos2d::Node *node);
	cocos2d::Vec2 _startPoint,_endPoint,_dir;
	float _delayTime;
};
#endif
