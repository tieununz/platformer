#ifndef __B2_HAZARDS_H__
#define __B2_HAZARDS_H__
#include "../b2ShapeCreatorDelegate.h"

class Hazards : public b2ShapeCreatorDelegate
{
public:
	virtual void collisionWith(b2PhysicObject* physicObject) {};
protected:
	bool    _ignoreHazards;
};
#endif
