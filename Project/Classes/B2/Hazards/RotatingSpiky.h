#ifndef __ROTATING_SPIKY_H__
#define __ROTATING_SPIKY_H__
#include "Hazards.h"

class RotatingSpiky : public Hazards
{
public:
	RotatingSpiky(void);
	virtual ~RotatingSpiky(void);
	static RotatingSpiky* create(cocos2d::ValueMap& properties,b2World* world,cocos2d::Node *node);
	virtual void collisionWith(b2PhysicObject* physicObject);
private:
	void initPhysic(cocos2d::ValueMap& properties,b2World* world,cocos2d::Node *node);

};
#endif
