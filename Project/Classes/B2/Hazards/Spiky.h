#ifndef __SPIKY_H__
#define __SPIKY_H__
#include "Hazards.h"

class Spiky : public Hazards
{
public:
	Spiky(void);
	virtual ~Spiky(void);
	static Spiky* create(cocos2d::ValueMap& properties,b2World* world,cocos2d::Node *node);
	virtual void collisionWith(b2PhysicObject* physicObject);
private:
	void initPhysic(cocos2d::ValueMap& properties,b2World* world,cocos2d::Node *node);

};
#endif
