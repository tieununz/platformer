#ifndef __B2_REWARD_H__
#define __B2_REWARD_H__
#include "../b2ShapeCreatorDelegate.h"

class Reward : public b2ShapeCreatorDelegate
{
public:
	virtual void collisionWith(Player* player) {};
};
#endif
