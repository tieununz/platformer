#ifndef __COIN_H__
#define __COIN_H__
#include "Reward.h"

class Coin : public Reward
{
public:
	Coin(void);
	virtual ~Coin(void);
	static Coin* create(cocos2d::ValueMap& properties,b2World* world,cocos2d::Node *node);
	void collisionWith(Player* player);
private:
	void initPhysic(cocos2d::ValueMap& properties,b2World* world,cocos2d::Node *node);
};
#endif
