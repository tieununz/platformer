#include "b2NodeManager.h"
#include "../Common/GameConfig.h"
#include "../Common/ResourceDefine.h"
#include "b2PhysicObject.h"
#define PTM_RATIO 32.0
b2NodeManager::b2NodeManager(void)
{
	accumulator = currentTime = lastTickTime = 0.0;
	_nodes = std::make_shared<Vector<b2Node*>>();
	_deleteNodes = std::make_shared<Vector<b2Node*>>();
	//_deleteBodies = std::make_shared<Vector<b2Body*>>();

	_collisionListener = nullptr;
}


b2NodeManager::~b2NodeManager(void)
{
	_world = nullptr;
	_mainLayer = nullptr;
	_collisionListener = nullptr;

	_nodes->clear();
	_deleteNodes->clear();
	_deleteBodies.clear();
}

static b2NodeManager *m_pInstance;

b2NodeManager* b2NodeManager::getInstance()
{
	if (m_pInstance == 0) {
		m_pInstance = new b2NodeManager();
	}
	return m_pInstance;
}

void b2NodeManager::addB2Node(b2Node *node)
{
	_nodes->pushBack(node);
	
}

void b2NodeManager::removeB2Node(b2Node *node)
{
	_nodes->eraseObject(node,true);
}

void b2NodeManager::addDeleteNode(b2Node *node)
{
	_deleteNodes->pushBack(node);
}

void b2NodeManager::addDeleteBody(b2Body *body)
{
	_deleteBodies.push_back(body);
}

void b2NodeManager::deleteNode(Vec2 pos)
{
	int size = _nodes->size();
	b2PhysicObject * node;

	for (int i = 0; i < size; i++) {
		node = (b2PhysicObject *) _nodes->at(i);
		if(node->getBoundingBox().containsPoint(pos))
		{
			this->addDeleteNode(node);
		}
	}
}

void b2NodeManager::clearAll()
{
	_nodes->clear();
	_deleteNodes->clear();
	_deleteBodies.clear();
}


void b2NodeManager::update(float dt)
{
	updatePhysic(dt);
	int size = _nodes->size();
	b2PhysicObject * node;

	for (int i = 0; i < size; i++) {
		node = (b2PhysicObject *) _nodes->at(i);
		if (node->isVisible())
			node->update(dt);
	}

	for (int i = 0; i < _deleteNodes->size(); i++) {
		removeB2Node(_deleteNodes->at(i));
		_world->DestroyBody(_deleteNodes->at(i)->getBody());
		_deleteNodes->at(i)->removeFromParentAndCleanup(true);
	}
	_deleteNodes->clear();

	for (int i = 0; i < _deleteBodies.size(); i++) {
		Sprite* _node = (Sprite *)_deleteBodies.at(i)->GetUserData();
		_node->removeFromParentAndCleanup(true);
		_world->DestroyBody(_deleteBodies.at(i));
	}
	_deleteBodies.clear();
	_deleteNodes->clear();
}

const double kSecondsPerUpdate = 0.017;

double getCurrentTimeInSeconds()
{
	static struct timeval currentTime;
	gettimeofday(&currentTime, nullptr);
	return (currentTime.tv_sec) + (currentTime.tv_usec / 1000000.0);
}

void b2NodeManager::updatePhysic(float dt)
{
	// get current time double
	currentTime = getCurrentTimeInSeconds();
	if (!lastTickTime)
		lastTickTime = currentTime;

	// determine the amount of time elapsed since our last update
	double frameTime = currentTime - lastTickTime;
	accumulator += frameTime;

	// update the world with the same seconds per update
	while (accumulator > kSecondsPerUpdate)
	{
		accumulator -= kSecondsPerUpdate;

		// perform a single step of the physics simulation
		_world->Step(kSecondsPerUpdate, 8, 1);
	}
	lastTickTime = currentTime;
}