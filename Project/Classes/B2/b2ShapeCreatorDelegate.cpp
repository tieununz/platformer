#include "b2ShapeCreatorDelegate.h"
#include "../Common/GameConfig.h"
#include "../Common/PRKit/PRFilledPolygon.h"

USING_NS_CC;

#define PTM_RATIO 32

FixtureDef* b2ShapeCreatorDelegate::createFixture(cocos2d::ValueMap object)
{
	int fixtureType = RECT_FIXTURE;
	for(auto propObj : object)
	{
		if(propObj.first == "points") {
			fixtureType = POLYGON_FIXTURE;
		} else if(propObj.first == "polylinePoints") {
			fixtureType = POLYLINE_FIXTURE;
		}
	}
	if(object["_type"].asString() == "Circle") {
		fixtureType = CIRCLE_FIXTURE;
	}


	if(fixtureType == POLYGON_FIXTURE) {
		return createPolygon(object);
	} else if(fixtureType == POLYLINE_FIXTURE) {
		return createPolyline(object);
	} else if(fixtureType == CIRCLE_FIXTURE) {
		return createCircle(object);
	} else if(fixtureType == RECT_FIXTURE) {
		return createRect(object);
	}
	return NULL;
}

FixtureDef* b2ShapeCreatorDelegate::createPolygon(ValueMap object)
{
	ValueVector pointsVector = object["points"].asValueVector();
	float width = object["width"].asFloat() / PTM_RATIO;
	float height = object["height"].asFloat() / PTM_RATIO;
	auto position = Vec2(-width/2, -height/2);

	b2PolygonShape *polyshape = new b2PolygonShape();
	b2Vec2 vertices[b2_maxPolygonVertices];
	int vindex = 0;

	if(pointsVector.size() > b2_maxPolygonVertices) {
		CCLOG("Skipping TMX polygon at x=%d,y=%d for exceeding %d vertices", object["x"].asInt(), object["y"].asInt(), b2_maxPolygonVertices);
		return NULL;
	}

	auto fix = new FixtureDef();

	for(Value point : pointsVector) {
		vertices[vindex].x = (point.asValueMap()["x"].asFloat() / PTM_RATIO + position.x);
        vertices[vindex].y = (-point.asValueMap()["y"].asFloat() / PTM_RATIO + position.y);
		vindex++;
	}

	polyshape->Set(vertices, vindex);
	fix->fixture.shape = polyshape;

	return fix;
}


FixtureDef* b2ShapeCreatorDelegate::createPolyline(ValueMap object)
{
	ValueVector pointsVector = object["polylinePoints"].asValueVector();
	float width = object["width"].asFloat() / PTM_RATIO;
	float height = object["height"].asFloat() / PTM_RATIO;
	auto position = Vec2(-width/2, -height/2);

	b2ChainShape *polylineshape = new b2ChainShape();
	
	int verticesCapacity=32;
	b2Vec2* vertices = (b2Vec2*)calloc(verticesCapacity, sizeof(b2Vec2));

	int vindex = 0;

	auto fix = new FixtureDef();

	for(Value point : pointsVector) {
		if(vindex>=verticesCapacity)
		{
			verticesCapacity+=32;
			vertices = (b2Vec2*)realloc(vertices, verticesCapacity*sizeof(b2Vec2));
		}
		vertices[vindex].x = (point.asValueMap()["x"].asFloat() / PTM_RATIO + position.x);
        vertices[vindex].y = (-point.asValueMap()["y"].asFloat() / PTM_RATIO + position.y);
		vindex++;
	}

	polylineshape->CreateChain(vertices, vindex);
	fix->fixture.shape = polylineshape;

	return fix;
}

FixtureDef* b2ShapeCreatorDelegate::createCircle(ValueMap object)
{
	float radius = object["width"].asFloat()/2 / PTM_RATIO;
	auto position = Vec2(-radius, -radius);

	b2CircleShape *circleshape = new b2CircleShape();
	circleshape->m_radius = radius;
	circleshape->m_p.Set(position.x + radius, position.y + radius);

	auto fix = new FixtureDef();
	fix->fixture.shape = circleshape;

	return fix;
}

FixtureDef* b2ShapeCreatorDelegate::createRect(ValueMap object)
{
	float width = object["width"].asFloat() / PTM_RATIO;
	float height = object["height"].asFloat() / PTM_RATIO;
	auto position = Vec2(-width/2, -height/2);
	b2PolygonShape *rectshape = new b2PolygonShape();
	b2Vec2 vertices[4];
	int vindex = 4;

	vertices[0].x = position.x + 0.0f;
	vertices[0].y = position.y + 0.0f;

	vertices[1].x = position.x + 0.0f;
	vertices[1].y = position.y + height;

	vertices[2].x = position.x + width;
	vertices[2].y = position.y + height;

	vertices[3].x = position.x + width;
	vertices[3].y = position.y + 0.0f;

	auto fix = new FixtureDef();
	rectshape->Set(vertices, vindex);
	fix->fixture.shape = rectshape;

	return fix;
}

void b2ShapeCreatorDelegate::addFilledPolygon(FixtureDef* fixtureShape,const std::string &path)
{
	const b2Shape* shape = fixtureShape->fixture.shape;
	if(shape->GetType() == b2Shape::e_polygon)
	{
		b2PolygonShape* polygon = (b2PolygonShape*)shape;
		int count = polygon->GetVertexCount();
		Vector2dVector polygonPoints;
		for( int i = 0; i < count; i++ )
		{
			polygonPoints.push_back(Vector2d(polygon->GetVertex(i).x * PTM_RATIO, polygon->GetVertex(i).y * PTM_RATIO));
		}
		Texture2D *texture = Director::getInstance()->getTextureCache()->addImage(path);
		PRFilledPolygon *filledPolygon = PRFilledPolygon::filledPolygonWithPointsAndTexture(polygonPoints, texture);
		addChild(filledPolygon);
	}
	if(shape->GetType() == b2Shape::e_chain)
	{
		b2ChainShape* polygon = (b2ChainShape*)shape;
		int count = polygon->m_count;

		Vector2dVector polygonPoints;
		for( int i = 0; i < count; i++ )
		{
			polygonPoints.push_back(Vector2d(polygon->m_vertices[i].x * PTM_RATIO, polygon->m_vertices[i].y * PTM_RATIO));
		}
		Texture2D *texture = Director::getInstance()->getTextureCache()->addImage(path);
		PRFilledPolygon *filledPolygon = PRFilledPolygon::filledPolygonWithPointsAndTexture(polygonPoints, texture);
		addChild(filledPolygon);
	}
	if(shape->GetType() == b2Shape::e_circle)
	{
		b2CircleShape* circle = (b2CircleShape*)shape;
		float radius = circle->m_radius;
		b2Vec2 center = circle->m_p;
		Vector2dVector polygonPoints;

		const float32 k_segments = 16.0f;
		int vertexCount=16;
		const float32 k_increment = 2.0f * b2_pi / k_segments;
		float32 theta = 0.0f;

		GLfloat*    glVertices = new (std::nothrow) GLfloat[vertexCount*2];
		for (int i = 0; i < k_segments; ++i)
		{
			b2Vec2 v = center + radius * b2Vec2(cosf(theta), sinf(theta));
			//glVertices[i*2]=v.x * PTM_RATIO;
			//glVertices[i*2+1]=v.y * PTM_RATIO;
			polygonPoints.push_back(Vector2d(v.x * PTM_RATIO, v.y * PTM_RATIO));

			theta += k_increment;
		}
		Texture2D *texture = Director::getInstance()->getTextureCache()->addImage(path);
		PRFilledPolygon *filledPolygon = PRFilledPolygon::filledPolygonWithPointsAndTexture(polygonPoints, texture);
		addChild(filledPolygon);
	}
}