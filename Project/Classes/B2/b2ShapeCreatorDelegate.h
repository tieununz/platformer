#ifndef __B2_SHAPE_D_H__
#define __B2_SHAPE_D_H__
#include "b2PhysicObject.h"

class FixtureDef;

class Player;
class b2ShapeCreatorDelegate : public b2PhysicObject
{
public:
	void addFilledPolygon(FixtureDef* fixtureShape,const std::string &path);
	FixtureDef* createFixture(cocos2d::ValueMap object);
public:
	FixtureDef* createPolygon(cocos2d::ValueMap object);
	FixtureDef* createPolyline(cocos2d::ValueMap object);
	FixtureDef* createCircle(cocos2d::ValueMap object);
	FixtureDef* createRect(cocos2d::ValueMap object);
};
#endif
