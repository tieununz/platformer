#ifndef __CONVEYER_BELT_H__
#define __CONVEYER_BELT_H__
#include "Platform.h"

class ConveyerBelt : public Platform
{
public:
	ConveyerBelt(void);
	virtual ~ConveyerBelt(void);
	static ConveyerBelt* create(cocos2d::ValueMap& properties,b2World* world,cocos2d::Node *node);
	virtual void collisionWith(b2PhysicObject* physicObject);

private:
	void initPhysic(cocos2d::ValueMap& properties,b2World* world,cocos2d::Node *node);
	b2Vec2 _impulse;
};
#endif
