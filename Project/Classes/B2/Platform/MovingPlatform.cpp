#include "MovingPlatform.h"
#include "../Player.h"
#include "../../Common/GameConfig.h"
#define PTM_RATIO 32.0
USING_NS_CC;
MovingPlatform::MovingPlatform(void)
{
}

MovingPlatform::~MovingPlatform(void)
{
}

MovingPlatform* MovingPlatform::create(cocos2d::ValueMap& properties,b2World* world,Node *node)
{
	MovingPlatform * sprite = new MovingPlatform();
	if (sprite) {
		sprite->initPhysic(properties,world,node);
		sprite->autorelease();
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}

void MovingPlatform::initPhysic(cocos2d::ValueMap& properties,b2World* world,Node *node)
{
	_world = world;
	_type = kPlatform;
	Size size = Size(properties["width"].asFloat(),properties["height"].asFloat());
	Vec2 position = Vec2(properties["x"].asFloat(),properties["y"].asFloat());
	position += size/2;

	_startPoint = Vec2(properties["Start_X"].asFloat(),properties["Start_Y"].asFloat());
	_endPoint = Vec2(properties["End_X"].asFloat(),properties["End_Y"].asFloat());
	_dir = _startPoint - _endPoint;
	_dir.normalize();
	_dir *= properties["Dir"].asFloat();
	_startPoint += position;
	_endPoint += position;
	_delayTime = properties["DelayTime"].asFloat();

	this->setPosition(position);
	this->setContentSize(size);
	node->addChild(this,_type,kPlatform);

	b2BodyDef bodyDef;
	bodyDef.type = b2_kinematicBody;
	bodyDef.position.Set(position.x / PTM_RATIO, position.y / PTM_RATIO);
	bodyDef.userData = this;
	bodyDef.fixedRotation = true;

	_body = _world->CreateBody(&bodyDef);

	FixtureDef* fixtureShape = createFixture(properties);
	if(fixtureShape != NULL) {
		fixtureShape->fixture.density = 1.0f;
		fixtureShape->fixture.friction = 0.7f;
		fixtureShape->fixture.restitution = 0.1f;
		fixtureShape->fixture.filter.categoryBits = kFilterCategoryPlatform;
		fixtureShape->fixture.filter.maskBits = 0xffff;
		_body->CreateFixture(&fixtureShape->fixture);
	}

	addFilledPolygon(fixtureShape,"pattern1.png");
	_body->SetLinearVelocity(b2Vec2(_dir.x/PTM_RATIO,_dir.y/PTM_RATIO));

	this->scheduleUpdate();
}

void MovingPlatform::update(float dt)
{
	Vec2 position = this->getPosition();
	if(_dir.x != 0)
	{
		if(position.x < _startPoint.x)
		{
			this->setPosition(Vec2(_startPoint.x,position.y));
			_body->SetLinearVelocity(b2Vec2(0,0));

			DelayTime* delayTime = DelayTime::create(_delayTime);
			CallFunc* callBack  = CallFunc::create([&](){
				_dir *= -1;
				_body->SetLinearVelocity(b2Vec2(_dir.x/PTM_RATIO,_dir.y/PTM_RATIO));
			});
			Sequence* seq = Sequence::create(delayTime, callBack,NULL);
			this->runAction(seq);

		}
		if(position.x > _endPoint.x)
		{
			this->setPosition(Vec2(_endPoint.x,position.y));
			_body->SetLinearVelocity(b2Vec2(0,0));

			DelayTime* delayTime = DelayTime::create(_delayTime);
			CallFunc* callBack  = CallFunc::create([&](){
				_dir *= -1;
				_body->SetLinearVelocity(b2Vec2(_dir.x/PTM_RATIO,_dir.y/PTM_RATIO));
			});
			Sequence* seq = Sequence::create(delayTime, callBack,NULL);
			this->runAction(seq);
		}
	}
	else
	{
		if(position.y < _startPoint.y)
		{
			this->setPosition(Vec2(position.x,_startPoint.y));
			_body->SetLinearVelocity(b2Vec2(0,0));

			DelayTime* delayTime = DelayTime::create(_delayTime);
			CallFunc* callBack  = CallFunc::create([&](){
				_dir *= -1;
				_body->SetLinearVelocity(b2Vec2(_dir.x/PTM_RATIO,_dir.y/PTM_RATIO));
			});
			Sequence* seq = Sequence::create(delayTime, callBack,NULL);
			this->runAction(seq);
		}
		if(position.y > _endPoint.y)
		{
			this->setPosition(Vec2(position.x,_endPoint.y));
			_body->SetLinearVelocity(b2Vec2(0,0));

			DelayTime* delayTime = DelayTime::create(_delayTime);
			CallFunc* callBack  = CallFunc::create([&](){
				_dir *= -1;
				_body->SetLinearVelocity(b2Vec2(_dir.x/PTM_RATIO,_dir.y/PTM_RATIO));
			});
			Sequence* seq = Sequence::create(delayTime, callBack,NULL);
			this->runAction(seq);
		}
	}
	b2PhysicObject::update(dt);
}