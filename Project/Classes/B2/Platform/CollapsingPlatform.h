#ifndef __COLLAPSING_PLATFORM_H__
#define __COLLAPSING_PLATFORM_H__
#include "Platform.h"

class CollapsingPlatform : public Platform
{
public:
	CollapsingPlatform(void);
	virtual ~CollapsingPlatform(void);
	static CollapsingPlatform* create(cocos2d::ValueMap& properties,b2World* world,cocos2d::Node *node);
	virtual void collisionWith(b2PhysicObject* physicObject);
private:
	void initPhysic(cocos2d::ValueMap& properties,b2World* world,cocos2d::Node *node);
	bool _isCollapsing;
	float _delayTime;
};
#endif
