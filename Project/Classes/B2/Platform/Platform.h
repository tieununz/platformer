#ifndef __B2_PLATFORM_H__
#define __B2_PLATFORM_H__
#include "../b2ShapeCreatorDelegate.h"

class Platform : public b2ShapeCreatorDelegate
{
public:
	virtual void collisionWith(b2PhysicObject* physicObject) {};
};
#endif
