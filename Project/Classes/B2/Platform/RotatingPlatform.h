#ifndef __ROTATING_PLATFORM_H__
#define __ROTATING_PLATFORM_H__
#include "Platform.h"

class RotatingPlatform : public Platform
{
public:
	RotatingPlatform(void);
	virtual ~RotatingPlatform(void);
	static RotatingPlatform* create(cocos2d::ValueMap& properties,b2World* world,cocos2d::Node *node);
private:
	void initPhysic(cocos2d::ValueMap& properties,b2World* world,cocos2d::Node *node);
};
#endif
