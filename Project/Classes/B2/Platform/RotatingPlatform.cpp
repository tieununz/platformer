#include "RotatingPlatform.h"
#include "../Player.h"
#include "../../Common/GameConfig.h"

#define PTM_RATIO 32.0
USING_NS_CC;
RotatingPlatform::RotatingPlatform(void)
{
}

RotatingPlatform::~RotatingPlatform(void)
{
}

RotatingPlatform* RotatingPlatform::create(cocos2d::ValueMap& properties,b2World* world,Node *node)
{
	RotatingPlatform * sprite = new RotatingPlatform();
	if (sprite) {
		sprite->initPhysic(properties,world,node);
		sprite->autorelease();
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}

void RotatingPlatform::initPhysic(cocos2d::ValueMap& properties,b2World* world,Node *node)
{
	_world = world;
	_type = kPlatform;
	Size size = Size(properties["width"].asFloat(),properties["height"].asFloat());
	Vec2 position = Vec2(properties["x"].asFloat(),properties["y"].asFloat());
	position += size/2;
	float time = properties["Time"].asFloat();
	float degree = properties["Degree"].asFloat();
	float delayTime = properties["DelayTime"].asFloat();
	bool reverse = properties["Reverse"].asBool();
	if(delayTime == 0.0f)
	{
		this->runAction(CCRepeatForever::create(RotateBy::create(time,degree)));
	}
	else
	{
		if(!reverse)
		{
			DelayTime* delayAction = DelayTime::create(delayTime);
			Sequence* seq = Sequence::create(RotateBy::create(time,degree), delayAction,NULL);
			this->runAction(CCRepeatForever::create(seq));
		}
		else
		{
			DelayTime* delayAction = DelayTime::create(delayTime);
			DelayTime* delayAction2 = DelayTime::create(delayTime);
			Sequence* seq = Sequence::create(RotateBy::create(time,degree), delayAction,RotateBy::create(time,-degree),delayAction2,NULL);
			this->runAction(CCRepeatForever::create(seq));
		}
	}
	this->setPosition(position);
	this->setContentSize(size);
	node->addChild(this,_type,kPlatform);

	b2BodyDef bodyDef;
	bodyDef.type = b2_kinematicBody;
	bodyDef.position.Set(position.x / PTM_RATIO, position.y / PTM_RATIO);
	bodyDef.userData = this;
	bodyDef.fixedRotation = true;

	_body = _world->CreateBody(&bodyDef);

	FixtureDef* fixtureShape = createFixture(properties);
	if(fixtureShape != NULL) {
		fixtureShape->fixture.density = 1.0f;
		fixtureShape->fixture.friction = 0.7f;
		fixtureShape->fixture.restitution = 0.1f;
		fixtureShape->fixture.filter.categoryBits = kFilterCategoryPlatform;
		fixtureShape->fixture.filter.maskBits = 0xffff;
		_body->CreateFixture(&fixtureShape->fixture);
	}

	addFilledPolygon(fixtureShape,"pattern1.png");
}