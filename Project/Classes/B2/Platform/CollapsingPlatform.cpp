#include "CollapsingPlatform.h"
#include "../Player.h"
#include "../../Common/GameConfig.h"
#include "../b2NodeManager.h"
#define PTM_RATIO 32.0
USING_NS_CC;
CollapsingPlatform::CollapsingPlatform(void)
{
	_isCollapsing = false;
}

CollapsingPlatform::~CollapsingPlatform(void)
{
}

CollapsingPlatform* CollapsingPlatform::create(cocos2d::ValueMap& properties,b2World* world,Node *node)
{
	CollapsingPlatform * sprite = new CollapsingPlatform();
	if (sprite) {
		sprite->initPhysic(properties,world,node);
		sprite->autorelease();
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}

void CollapsingPlatform::initPhysic(cocos2d::ValueMap& properties,b2World* world,Node *node)
{
	_world = world;
	_type = kPlatform;
	Size size = Size(properties["width"].asFloat(),properties["height"].asFloat());
	Vec2 position = Vec2(properties["x"].asFloat(),properties["y"].asFloat());
	position += size/2;
	_delayTime = properties["DelayTime"].asFloat();
	this->setPosition(position);
	this->setContentSize(size);
	node->addChild(this,_type,kPlatform);

	b2BodyDef bodyDef;
	bodyDef.type = b2_kinematicBody;
	bodyDef.position.Set(position.x / PTM_RATIO, position.y / PTM_RATIO);
	bodyDef.userData = this;
	bodyDef.fixedRotation = true;

	_body = _world->CreateBody(&bodyDef);

	FixtureDef* fixtureShape = createFixture(properties);
	if(fixtureShape != NULL) {
		fixtureShape->fixture.density = 1.0f;
		fixtureShape->fixture.friction = 0.7f;
		fixtureShape->fixture.restitution = 0.1f;
		fixtureShape->fixture.filter.categoryBits = kFilterCategoryPlatform;
		fixtureShape->fixture.filter.maskBits = 0xffff;
		_body->CreateFixture(&fixtureShape->fixture);
	}

	addFilledPolygon(fixtureShape,"pattern1.png");
}

void CollapsingPlatform::collisionWith(b2PhysicObject* physicObject)
{
	if(physicObject->getTag() != kPlayer)
		return;
	Player* _player = (Player *)physicObject;
	if(!_isCollapsing)
	{
		_isCollapsing = true;
		DelayTime* delayTime = DelayTime::create(_delayTime);
		CallFunc* callBack  = CallFunc::create([&](){
			b2NodeManager::getInstance()->addDeleteNode(this);
		});
		Sequence* seq = Sequence::createWithTwoActions(delayTime, callBack);
		this->runAction(seq);
	}
}