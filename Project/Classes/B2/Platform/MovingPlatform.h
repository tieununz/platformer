#ifndef __MOVING_PLATFORM_H__
#define __MOVING_PLATFORM_H__
#include "Platform.h"

class MovingPlatform : public Platform
{
public:
	MovingPlatform(void);
	virtual ~MovingPlatform(void);
	static MovingPlatform* create(cocos2d::ValueMap& properties,b2World* world,cocos2d::Node *node);
	virtual void update(float dt) override;
private:
	void initPhysic(cocos2d::ValueMap& properties,b2World* world,cocos2d::Node *node);
	cocos2d::Vec2 _startPoint,_endPoint,_dir;
	float _delayTime;
};
#endif
