#ifndef __B2_PLAYER_H__
#define __B2_PLAYER_H__
#include "b2PhysicObject.h"

class Player : public b2PhysicObject
{
public:
	Player(void);
	virtual ~Player(void);
	static Player* create(cocos2d::Vec2 position,b2World* world,cocos2d::Node *node);
	virtual void update(float dt) override;
	virtual void collisionWith(b2Contact* contact,b2PhysicObject* object);
private:
	void initPhysic(cocos2d::Vec2 position,b2World* world,cocos2d::Node *node);
public:
	void walkLeft(float dt);
	void walkRight(float dt);
	void jump();
	void loot();
private:
	int countJump;
	bool _hasLoot;
	bool _turnLeft;
	CC_SYNTHESIZE(b2PhysicObject*, _ball, Ball);
private:
	cocos2d::Sprite *_mainSprite;
	cocos2d::Action* walk;
	CC_SYNTHESIZE(int, _state, State);

public:
	void setJump();
	void setWalk();
	void setIdle();
	void setSlideWall();
	void setDie();
};
#endif
