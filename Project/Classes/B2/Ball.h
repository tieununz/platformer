#ifndef __B2_BALL_H__
#define __B2_BALL_H__
#include "b2PhysicObject.h"

class Ball : public b2PhysicObject
{
public:
	Ball(void);
	virtual ~Ball(void);
	static Ball* create(cocos2d::Vec2 position,b2World* world,cocos2d::Node *node);

private:
	void initPhysic(cocos2d::Vec2 position,b2World* world,cocos2d::Node *node);
};
#endif
