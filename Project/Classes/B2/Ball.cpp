#include "Ball.h"
#include "../Common/GameConfig.h"
#include "b2NodeManager.h"
#define PTM_RATIO 32.0
USING_NS_CC;
Ball::Ball(void)
{
}

Ball::~Ball(void)
{
}

Ball* Ball::create(cocos2d::Vec2 position,b2World* world,Node *node)
{
	Ball * sprite = new Ball();
	if (sprite) {
		sprite->initPhysic(position,world,node);
		sprite->autorelease();
		return sprite;
	}
	CC_SAFE_DELETE(sprite);
	return NULL;
}

void Ball::initPhysic(cocos2d::Vec2 position,b2World* world,Node *node)
{
	float radius = 0.7f;
	_world = world;
	_type = kBall;
	this->setPosition(position);
	this->setContentSize(Size(radius * 2 * PTM_RATIO,radius * 2 * PTM_RATIO));
	node->addChild(this,_type,kObject);

	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(position.x / PTM_RATIO, position.y / PTM_RATIO);
	bodyDef.userData = this;
	bodyDef.fixedRotation = true;

	_body = _world->CreateBody(&bodyDef);

	b2CircleShape circle;
	circle.m_radius = radius ;
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &circle;    
	fixtureDef.density = 1.0f;
	fixtureDef.friction = 0.7f;
	fixtureDef.restitution = 0.1f;
	fixtureDef.filter.categoryBits = kFilterCategoryBall;
	fixtureDef.filter.maskBits = 0xffff;
	_body->CreateFixture(&fixtureDef); 
	//_body->SetBullet(true);

	Sprite* mainSprite = Sprite::create("buttonYellow.png");
	this->addChild(mainSprite);

	this->unscheduleUpdate();
}